package fr.umlv.entity.user;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import fr.umlv.entity.img.Image;

@Named
@Stateless
public class UserAppController {

	// ======================================
	// = Attributes =
	// ======================================
	@PersistenceContext(unitName = "UPEMGUR_PU", type = PersistenceContextType.TRANSACTION)
	EntityManager em;

	private UserApp user;

	// ======================================
	// = Business methods =
	// ======================================

	public void init() {
		user = new UserApp();
	}

	public List<UserApp> findAllUser() {
		return em.createNamedQuery("findAllUser", UserApp.class)
				.getResultList();
	}

	public void doCreateUser() {
		em.persist(user);

	}

	public UserApp findUserById(Long id) {
		UserApp user = em.find(UserApp.class, id);
		return user;
	}

	public UserApp getUser() {
		return user;
	}

	public void setUser(UserApp user) {
		this.user = user;
	}

	/**
	 * this will check if the given user exist by matching username and login
	 *
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean isCrededentialOk(String username, String password) {

		Query q = em
				.createNativeQuery("SELECT * FROM userapp  where username='"
						+ username + "' and password='" + password + "'");
		// q.setParameter(1, username);
		// q.setParameter(2, password);
		List<UserApp> resultList = q.getResultList();
		return !resultList.isEmpty();
	}

	public UserApp getUserBy(String username, String password) {

		Query q = em.createNamedQuery("findUser", UserApp.class);
		q.setParameter("username", username);
		q.setParameter("password", password);

		List<UserApp> resultList = q.getResultList();

		if (resultList.isEmpty())
			return null;
		return resultList.get(0);
	}

	public boolean isUsernameNotAlreadyTakken(String username) {
		Query q = em
				.createQuery("SELECT b FROM UserApp b where b.username= ?1");
		List resultList = q.setParameter(1, username).getResultList();
		return resultList.isEmpty();
	}

	public void deleteUser() {
		UserApp toRemove = em.merge(this.user);
		em.remove(toRemove);
	}
	public void deleteUser(UserApp user) {
		UserApp toRemove = em.merge(user);
		em.remove(toRemove);
	}

}
