/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.umlv.entity.user;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import fr.umlv.entity.img.Image;

/**
 *
 * @author emmanuel
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "findAllUser", query = "SELECT b FROM UserApp b"),
		@NamedQuery(name = "findUser", query = "SELECT b FROM UserApp b  WHERE b.username= :username and b.password= :password"), })
public class UserApp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull(message = "Ce champ doit etre renseigne")
	@Size(min = 1, max = 50, message = "ce champ doit contenir entre 1 et 50 caracteres")
	private String name;

	@NotNull(message = "Ce champ doit etre renseigne")
	@Size(min = 3, max = 25, message = "ce champ doit contenir entre 3 et 25 caracteres")
	private String username;

	@NotNull(message = "Ce champ doit etre renseigne")
	@Size(min = 5, message = "Le mot de passe doit contenir au moins 5 caracteres")
	private String password;

	@NotNull(message = "Ce champ doit etre renseigne")
	@Pattern(regexp = "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)", message = "Merci de saisir une adresse mail valide")
	private String email;

	private String phoneNumber;

	@Enumerated(EnumType.STRING)
	private UserRole role;

	@OneToMany(mappedBy = "owner",cascade = CascadeType.ALL)
	List<Image> imageList;

	public UserApp() {
	}

	public UserApp(String name, String username, String password, String email,
			String phoneNumber) {
		this.name = name;
		this.username = username;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
		role = UserRole.BASIC;
	}

	public UserApp(String name, String username, String password, String email,
			String phoneNumber, UserRole role) {
		
		this.name = name;
		this.username = username;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.role = role;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public List<Image> getImageList() {
		return imageList;
	}

	public void setImageList(List<Image> imageList) {
		this.imageList = imageList;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
