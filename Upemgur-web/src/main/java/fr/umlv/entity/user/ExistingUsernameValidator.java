/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.umlv.entity.user;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

/**
 *
 * @author emmanuel
 */

@FacesValidator(value = "existingUsernameValidator")
public class ExistingUsernameValidator implements Validator {

	@Inject
	UserAppController userAppController;

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		String username = (String) value;
		if (username == null || username.length() <= 0) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Ce champ ne peut pas etre vide", null));
		} else if (!userAppController.isUsernameNotAlreadyTakken(username)) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Ce nom d'utilisateur a déjà été pris", null));

		}

	}

}
