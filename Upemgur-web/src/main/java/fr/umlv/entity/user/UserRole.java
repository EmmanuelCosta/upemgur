package fr.umlv.entity.user;

public enum UserRole {
	ADMIN, PREMIUM, BASIC;
}
