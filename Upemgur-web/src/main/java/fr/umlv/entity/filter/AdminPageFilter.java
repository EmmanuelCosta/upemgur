package fr.umlv.entity.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.umlv.entity.user.UserApp;

public class AdminPageFilter implements Filter {

	String adminPage = "secure/displayUser.jsp";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		/* Cast des objets request et response */
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();
		UserApp user = (UserApp) session.getAttribute("USER");

		if (user == null) {
			response.sendRedirect(request.getContextPath() + "/"
					+ "connexion.jsp");
		} else if (!user.getRole().toString().equalsIgnoreCase("ADMIN")) {
			response.sendRedirect(request.getContextPath() + "/accueil.jsp");
		} else {
			chain.doFilter(request, response);
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}
}
