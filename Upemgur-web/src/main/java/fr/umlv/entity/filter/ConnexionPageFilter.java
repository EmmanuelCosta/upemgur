package fr.umlv.entity.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.umlv.entity.user.UserApp;

public class ConnexionPageFilter implements Filter {

	String connectPage = "accueil.jsp";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		/* Cast des objets request et response */
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession();
		UserApp user = (UserApp) session.getAttribute("USER");
		if (user == null) {
			chain.doFilter(request, response);
			
		} else {
			response.sendRedirect(request.getContextPath() + "/" + connectPage);
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
