package fr.umlv.entity.img;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

@Named
@Stateless
public class ImageController {

	// ======================================
	// = Attributes =
	// ======================================
	@PersistenceContext(unitName = "UPEMGUR_PU", type = PersistenceContextType.TRANSACTION)
	EntityManager em;

	public static int LIMIT = 12;
	
	private Image image;

	// ======================================
	// = Business methods =
	// ======================================
	
	public void setPicture(Image image) {
		this.image = image;
	}
	
	public Image getImage() {
		return image;
	}

	public void doCreatePicture() {
		em.persist(image);
	}

	public Image doUpdatePicture() {
		return em.merge(image);
	}

	public void doRemovePicture() {
		Image toRemove = em.merge(image);
		em.remove(toRemove);
	}

	public Image findPictureById(Long id) {
		return em.find(Image.class, id);
	}
	
	public List<Image> findAllPicturesThatContain(String title) {
		Query q = em.createQuery("Select i From Image i WHERE i.title LIKE '%" + title + "%' AND i.visibility=:visibility");
		q.setParameter("visibility", ImageVisibility.PUBLIC);
		q.setMaxResults(LIMIT);
		try {
			List<Image> result = q.getResultList();
			return result;
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Image> findAllPictures() {
		Query q = em.createQuery("Select i From Image i");
		return q.getResultList();
	}

	public List<Image> findAllPicturesFromUser(long id) {
		Query q = em.createQuery("Select i From Image i WHERE i.owner.id=" + id);
		try {
			List<Image> result = q.getResultList();
			return result;
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Image> findAllPublicPictures() {
		Query q = em
				.createQuery("Select i From Image i WHERE i.visibility=:visibility");
		q.setParameter("visibility", ImageVisibility.PUBLIC);
		try {
			List<Image> result = q.getResultList();
			return result;
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<Image> findLastUploadedPublicPicturesFromOthers(long id) {
		
		Query q = em
				.createQuery("Select i From Image i WHERE i.owner.id<>:id" + " AND i.visibility=:visibility");
		q.setParameter("id", id);
		q.setParameter("visibility", ImageVisibility.PUBLIC);
		q.setMaxResults(LIMIT);
		try {
			List<Image> result = q.getResultList();
			return result;
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
	 * Permet de vérifier qu'un titre d'image n'existe pas déjà pour un user
	 * donné
	 */
	public boolean isPictureTitleAvailable(long id, String title) {
		Query q = em
				.createQuery("Select i From Image i WHERE i.owner.id="+ id + " AND i.title='"+ title + "'");
		List<Image> result = q.getResultList();

		return result.isEmpty();
	}

}
