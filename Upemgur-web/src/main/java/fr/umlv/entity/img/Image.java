package fr.umlv.entity.img;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import fr.umlv.entity.user.UserApp;

@Entity
public class Image implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	private String title;

	@NotNull
	private String pathToFile;

	@NotNull
	private String uploadDate;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "owner_id")
	private UserApp owner;

	@Enumerated(EnumType.STRING)
	private ImageVisibility visibility;

	private String description;

	private Double length;

	private Long height;

	private Long width;

	// see
	// https://tika.apache.org/1.7/api/org/apache/tika/metadata/TIFF.html#ORIENTATION

	// chemin du fichier sur le serveur

	private String resolution_unit;

	private Double yResolution;

	private Double xResolution;

	private Integer bitsPerSample;

	public Image() {
	}

	public Image(String title, String description, String uploadTime,
			UserApp owner, ImageVisibility visibility) {

		this.title = title;
		this.description = description;
		this.uploadDate = uploadTime;
		this.owner = owner;
		this.visibility = visibility;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPathToFile() {
		return pathToFile;
	}

	public void setPathToFile(String pathToFile) {
		this.pathToFile = pathToFile;
	}

	public String getUploadTime() {
		return uploadDate;
	}

	public void setUploadDate(String uploadTime) {
		this.uploadDate = uploadTime;
	}

	public UserApp getOwner() {
		return owner;
	}

	public void setOwner(UserApp owner) {
		this.owner = owner;
	}

	public ImageVisibility getVisibility() {
		return visibility;
	}

	public void setVisibility(ImageVisibility visibility) {
		this.visibility = visibility;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}

	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	public String getResolution_unit() {
		return resolution_unit;
	}

	public void setResolution_unit(String resolution_unit) {
		this.resolution_unit = resolution_unit;
	}

	public Double getyResolution() {
		return yResolution;
	}

	public void setyResolution(Double yResolution) {
		this.yResolution = yResolution;
	}

	public Double getxResolution() {
		return xResolution;
	}

	public void setxResolution(Double xResolution) {
		this.xResolution = xResolution;
	}

	public Integer getBitsPerSample() {
		return bitsPerSample;
	}

	public void setBitsPerSample(Integer bitsPerSample) {
		this.bitsPerSample = bitsPerSample;
	}

}
