package fr.umlv.entity.img;

public enum ImageVisibility {
	PRIVATE, PUBLIC;
}
