package fr.umlv.entity.img;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;


/**
 *
 * @author thomas
 */

@FacesValidator(value = "existingPictureValidator")
public class ExistingPictureValidator implements Validator {

	@Inject
	ImageController imageController;

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		String pictureInfos = (String) value;

		int id = Integer.valueOf(pictureInfos.split(";")[0]);
		String pictureTitle = pictureInfos.split(";")[1];

		if (pictureTitle == null || pictureTitle.length() <= 0) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Ce champ ne peut pas être vide", null));
		} else if (!imageController.isPictureTitleAvailable(id, pictureTitle)) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Cette image existe déjà", null));

		}
	}
}
