package fr.umlv.entity.img;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.jpeg.JpegParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import fr.umlv.entity.user.UserApp;

public class ImageBuilder {

	public ImageBuilder() {
	}

	public static Image createImage(String pathToFile, String title,
			String description, ImageVisibility imageVisibility, UserApp owner)
			throws FileNotFoundException {

		Image image = new Image();

		Parser parser = new JpegParser();

		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		FileInputStream inputstream = new FileInputStream(new File(pathToFile));
		ParseContext pcontext = new ParseContext();

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		// get current date time with Date()
		Calendar cal = Calendar.getInstance();
		String currentDate = dateFormat.format(cal.getTime());
		image.setVisibility(imageVisibility);
		image.setTitle(title);
		image.setDescription(description);
		image.setPathToFile(pathToFile);
		image.setUploadDate(currentDate);
		image.setOwner(owner);

		try {
			parser.parse(inputstream, handler, metadata, pcontext);
			// String[] metadataNames = metadata.names();

			// get data with metadata extractor
			String data = metadata.get("Resolution Units");
			if (data == null) {
				data = metadata.get("tiff:ResolutionUnit");

			}
			image.setResolution_unit(data);

			data = metadata.get("tiff:XResolution");
			if (data == null) {
				data = metadata.get("X Resolution");
				if (data == null) {
					data = "0";
				}
				data = data.split(" ")[0];
			}
			image.setxResolution(new Double(data));

			data = metadata.get("tiff:YResolution");
			if (data == null) {
				data = metadata.get("Y Resolution");
				if (data == null) {
					data = "0";
				}
				data = data.split(" ")[0];
			}
			image.setyResolution(new Double(data));

			data = metadata.get("tiff:BitsPerSample");
			if (data == null) {
				data = "0";
			}
			image.setBitsPerSample(new Integer(data));

			data = metadata.get("tiff:ImageWidth");
			if (data == null) {
				data = metadata.get("Image Width");
				if (data == null) {
					data = "0";
				}
				data = data.split(" ")[0];
			}
			image.setWidth(new Long(data));

			data = metadata.get("tiff:ImageHeight");
			if (data == null) {
				data = metadata.get("Image Height");
				if (data == null) {
					data = "0";
				}
				data = data.split(" ")[0];
			}
			image.setHeight(new Long(data));

			data = metadata.get("tiff:ImageLength");
			if (data == null) {
				data = metadata.get("Image Length");
				if (data == null) {
					data = "0";
				}
				data = data.split(" ")[0];
			}
			image.setLength(new Double(data));

			// uncomment this to debug purpose

			// for (String name : metadataNames) {
			//
			// System.out.println(name + ": " + metadata.get(name));
			//
			// }
			return image;
		} catch (IOException | SAXException | TikaException e) {
			image.setResolution_unit("NOT_DEFINED");
			image.setxResolution(new Double(-1));
			image.setyResolution(new Double(-1));
			image.setBitsPerSample(new Integer(-1));
			image.setWidth(new Long(-1));
			image.setWidth(new Long(-1));
			image.setHeight(new Long(-1));
			image.setLength(new Double(-1));

			return image;
		}
	}

	public static boolean isTypeSupported(String pathToFile) {
		try {
			Parser parser = new JpegParser();
			BodyContentHandler handler = new BodyContentHandler();
			Metadata metadata = new Metadata();
			FileInputStream inputstream;

			inputstream = new FileInputStream(new File(pathToFile));
			ParseContext pcontext = new ParseContext();

			parser.parse(inputstream, handler, metadata, pcontext);
			return true;
		} catch (IOException | SAXException | TikaException e) {
			return false;
		}

	}
}
