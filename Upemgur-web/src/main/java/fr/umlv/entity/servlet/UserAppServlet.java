/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.umlv.entity.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;

import fr.umlv.entity.user.UserApp;
import fr.umlv.entity.user.UserAppController;
import fr.umlv.entity.user.UserRole;

/**
 *
 * @author emmanuel
 */
@WebServlet("/InscriptionServlet")
public class UserAppServlet extends HttpServlet {

	@Inject
	UserAppController userController;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String nom = (String) req.getParameter("nom");
		String username = (String) req.getParameter("username");
		String email = (String) req.getParameter("email");
		String pwd = (String) req.getParameter("pwd");
		String phone = (String) req.getParameter("phone");
		String typeCompte = (String) req.getParameter("typeCompte");

		boolean error = false;
		if (checkValideString(nom)) {
			resp.addHeader("nom", "Le nom ne peut etre vide");
			// System.out.println("error detected for " + nom);
			error = true;
		}
		if (checkValideString(username)
				|| !userController.isUsernameNotAlreadyTakken(username)) {
			resp.addHeader("username", "Ce username existe deja");
			// System.out.println("error detected for " + username +
			// " it exist = " +
			// userAppController.isUsernameNotAlreadyTakken(username));
			error = true;
		}
		if (checkValideString(email)
				|| !email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
			resp.addHeader("email", "veuillez entrer un email correcte");
			// System.out.println("error detected for " + email);
			error = true;
		}
		if (checkValideString(pwd) || pwd.length() <= 5) {
			// System.out.println("error detected for " + pwd);
			resp.addHeader("pwd",
					"Mot de passe obligatoire et doit contenir au moins 5 caractere");
			error = true;
		}
		
		if(checkValideString(typeCompte) || UserRole.valueOf(typeCompte) == null){
			typeCompte=UserRole.BASIC.name();
		}else if(typeCompte.equalsIgnoreCase("ADMIN")){
			resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Vous n'êtes pas autorisée à créer un utilisateur ADMIN");
			error=true;
		}
		if (phone == null || phone.isEmpty()) {
			
			phone = "0";
		}
		if (error) {

			resp.sendError(400);

		}else {
			// new UserApp(nom, username, pwd, email, phone);
			UserApp user = userController.getUser();
			user.setName(nom);
			user.setPassword(DigestUtils.sha1Hex(pwd));
			user.setUsername(username);
			user.setEmail(email);
			user.setPhoneNumber(phone);
			UserRole role = UserRole.valueOf(typeCompte);
			user.setRole(role);
			userController.doCreateUser();
			
		}
	}

	private boolean checkValideString(String toTest) {
		return (toTest == null || toTest.isEmpty());
	}

}
