package fr.umlv.entity.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.umlv.entity.user.UserApp;
import fr.umlv.entity.user.UserAppController;

/**
 * Servlet implementation class DeleteUserServlet
 */
@WebServlet("/admin/DeleteUserServlet")
public class DeleteUserServlet extends HttpServlet {
	@Inject
	UserAppController userController;

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserApp user = (UserApp) session.getAttribute("USER");
		Long id = new Long(request.getParameter("id"));

		if (user == null) {
			response.sendRedirect(request.getContextPath() + "/"
					+ "connexion.jsp");
		} else if (!user.getRole().toString().equalsIgnoreCase("ADMIN")) {
			response.sendRedirect(request.getContextPath() + "/accueil.jsp");
		} else {

			UserApp userToRemove = userController.findUserById(id);
			userController.deleteUser(userToRemove);
		}

	}

}
