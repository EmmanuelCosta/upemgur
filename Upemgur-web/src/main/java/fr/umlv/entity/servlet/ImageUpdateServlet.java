package fr.umlv.entity.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.umlv.entity.img.Image;
import fr.umlv.entity.img.ImageController;
import fr.umlv.entity.img.ImageVisibility;
import fr.umlv.entity.user.UserApp;

/**
 * Servlet implementation class ImageUpdateServlet
 */
@WebServlet("/secure/ImageUpdateServlet")
public class ImageUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	ImageController imageController;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ImageUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserApp user = (UserApp) request.getSession().getAttribute("USER");
		if (user == null) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Vous n'êtes pas authentifié");
			return;
		}

		String idPicture = request.getParameter("idPicture");
		String idUser = request.getParameter("idUser");
		String newVisibility = request.getParameter("newVisibility");

		if(idPicture == null || idUser == null || newVisibility == null)
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Un des paramètres est incorrect.");

		else if(new Long(idUser) != user.getId())
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "Vous n'avez pas les droits pour changer la visibilité de cette image.");				

		else if(new Long(idUser) == user.getId()) {
			Image i = imageController.findPictureById(new Long(idPicture));
			
			if(newVisibility.equalsIgnoreCase("PUBLIC"))
				i.setVisibility(ImageVisibility.PUBLIC);
			else if(newVisibility.equalsIgnoreCase("PRIVATE"))
				i.setVisibility(ImageVisibility.PRIVATE);
			
			imageController.setPicture(i);
			imageController.doUpdatePicture();
			response.setStatus(HttpServletResponse.SC_OK);
		}
	}	
}
