package fr.umlv.entity.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.umlv.entity.img.Image;
import fr.umlv.entity.img.ImageController;
import fr.umlv.entity.user.UserApp;
import fr.umlv.entity.user.UserAppController;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/secure/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	UserAppController userAppController;

	@Inject
	ImageController imageController;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("title");
		Long userId = new Long(request.getParameter("userId"));

		title = title.replaceAll(" +", "+").trim();

		if (userId == null || title == null || title.isEmpty())
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Un des paramètres n'est pas correct");

		List<Image> result = new ArrayList<Image>(imageController.findAllPicturesThatContain(title));  
		UserApp userSearching = userAppController.findUserById(userId);

		StringBuilder buildResult = new StringBuilder();

		buildResult.append("<h4 class=\"page-header\">Résultats de votre recherche"
				+ "<small class=\"pull-right\" style=\"color: black;\">"
				+ result.size() + " résultat(s).</small></h4>"
				+ "<ul class=\"list-inline\">");

		if(result.isEmpty())
			buildResult.append("<p class=\"text-info\">Votre recherche n'a retourné aucun résultat.</p>");

		else
			for(Image i: result){
				buildResult.append("<li class=\"col-md-2\" style=\"list-style: none; margin-bottom: 25px;\">"
						+ "<img alt=\"" + i.getId() + ";" + userSearching.getId() + ";" + i.getOwner().getUsername() + ";" + userSearching.getUsername() + ";"
						+ userSearching.getRole() + ";" + i.getDescription() + ";" + i.getHeight() + ";" + i.getWidth() + ";" + i.getVisibility() + "\" class=\"img-thumbnail\""
						+ " src=\"/Upemgur-web/uploadFiles/" + i.getOwner().getId() + "/" + i.getTitle() + "\""
						+ " title=\"" + i.getTitle() + "\" style=\"cursor: pointer; height: 175px; width: 200px\""
						+ " onclick=\"drawImageModal(this.src, this.title, this.alt);\"></img></li>");
			}

		buildResult.append("</ul>");

		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(buildResult.toString());
		response.setStatus(HttpServletResponse.SC_OK);

	}

}
