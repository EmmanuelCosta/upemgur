package fr.umlv.entity.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DeconnectionServlet
 */
@WebServlet("/DeconnectionServlet")
public class DeconnectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String URL_REDIRECTION = "accueil.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeconnectionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("USER") != null) {
			session.removeAttribute("USER");
			response.setStatus(200);
		} else {
			response.sendError(400, "Utilisateur déja déconnecté");

		}

	};

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
