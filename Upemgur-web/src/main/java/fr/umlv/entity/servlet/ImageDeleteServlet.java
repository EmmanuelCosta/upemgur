package fr.umlv.entity.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.umlv.entity.img.Image;
import fr.umlv.entity.img.ImageController;
import fr.umlv.entity.user.UserApp;

/**
 * Servlet implementation class ImageDeleteServlet
 */
@WebServlet(urlPatterns = "/secure/ImageDeleteServlet")
public class ImageDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	ImageController imageController;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ImageDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doPost(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		UserApp user = (UserApp) request.getSession().getAttribute("USER");
		if (user == null) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Vous n'êtes pas authentifié");
			return;
		}

		String idPicture = request.getParameter("idPicture");
		String idUser = request.getParameter("idUser");

		if(idPicture == null || idUser == null)
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Un des paramètres est incorrect.");

		else if(new Long(idUser) != user.getId())
			response.sendError(HttpServletResponse.SC_FORBIDDEN, "Vous n'avez pas les droits pour supprimer cette image.");				

		else if(new Long(idUser) == user.getId()) {
			Image i = imageController.findPictureById(new Long(idPicture));
			imageController.setPicture(i);
			imageController.doRemovePicture();
			response.setStatus(200);
		}
	}
}
