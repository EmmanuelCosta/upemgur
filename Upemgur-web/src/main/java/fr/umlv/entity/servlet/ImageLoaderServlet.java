package fr.umlv.entity.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import fr.umlv.entity.img.Image;
import fr.umlv.entity.img.ImageBuilder;
import fr.umlv.entity.img.ImageController;
import fr.umlv.entity.img.ImageVisibility;
import fr.umlv.entity.user.UserApp;

@WebServlet(urlPatterns = "/secure/ImageLoaderServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
maxFileSize = 1024 * 1024 * 10, // 10MB
maxRequestSize = 1024 * 1024 * 50)
// 50MB
public class ImageLoaderServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String SAVE_DIR = "uploadFiles";
	private final String fileSeparator = File.separator;

	@Inject
	private ImageController imageController;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		try {
			doPost(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		if (isMultipart) {
			UserApp user = (UserApp) request.getSession().getAttribute("USER");
			if (user == null) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
						"Vous n'êtes pas authentifié");
				return;
			}

			// Create a factory for disk-based file items
			FileItemFactory factory = new DiskFileItemFactory();

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			String title = "";
			String desc = "";
			try {
				// Parse the request
				List /* FileItem */items = upload.parseRequest(request);
				Iterator iterator = items.iterator();
				String root = getServletContext().getRealPath("/");
				StringBuilder pathname = new StringBuilder();
				pathname.append(root).append(SAVE_DIR).append(fileSeparator)
						.append(user.getId());
				// root + SAVE_DIR+"/"+user.getId();
				String fileName = "";
				String visibility = "";
				while (iterator.hasNext()) {
					FileItem item = (FileItem) iterator.next();
					if (!item.isFormField()) {

						fileName = item.getName();

						File path = new File(pathname.toString());
						if (!path.exists()) {
							boolean status = path.mkdirs();
						}

						File uploadedFile = new File(path + fileSeparator
								+ fileName);
						item.write(uploadedFile);
					} else {

						String fieldName = item.getFieldName();
						String fieldValue = item.getString();
						if (fieldName.equalsIgnoreCase("pictureTitle")) {
							title = fieldValue;
						} else if (fieldName.equalsIgnoreCase("pictureDesc")) {
							desc = fieldValue;
						} else if (fieldName
								.equalsIgnoreCase("imageVisibility")) {

							visibility = fieldValue;
						}
					}
				}
				Image image = ImageBuilder.createImage(pathname.toString()
						+ fileSeparator + fileName, title, desc,
						ImageVisibility.valueOf(visibility), user);
				// Logger.getAnonymousLogger().info(image.getId()+
				// " = "+image.getPathToFile()+" = "+image.getTitle()+" = "+image.getUploadTime());
				imageController.setPicture(image);
				imageController.doCreatePicture();

			} catch (FileUploadException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
