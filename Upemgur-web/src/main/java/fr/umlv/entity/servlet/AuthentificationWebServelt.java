/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.umlv.entity.servlet;

/**
 *
 * @author Thomas
 */
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;

import fr.umlv.entity.user.UserApp;
import fr.umlv.entity.user.UserAppController;

/**
 * Servlet implementation class Controleur
 */

@WebServlet("/AuthentificationWebServelt")
public class AuthentificationWebServelt extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private UserAppController userController;

	// Si on tente d'accéder directement à la Servlet, on redirige vers
	// index.jsp
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String login = request.getParameter("login");
		String pass = request.getParameter("password");
		response.setCharacterEncoding("UTF-8");

		if (!"".equals(login) && !"".equals(pass)) {
			String pwdCrypted = DigestUtils.sha1Hex(pass);
			if (userController.isCrededentialOk(login, pwdCrypted)) {

				UserApp user = userController.getUserBy(login, pwdCrypted);
				Long id = user.getId();
				HttpSession session = request.getSession();
		
				session.setAttribute("USER", user);

				response.setStatus(HttpServletResponse.SC_OK);
			} else {

				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		}
		// Si le formulaire est incomplet, renvoi vers le formulaire avec un
		// message d'erreur
		else {
			// response.setHeader("Authentification01",
			// "Vous devez renseigner les champs");
			// response.sendError(400);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
}
