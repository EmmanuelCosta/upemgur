$(document).ready(function() {
	$('#connexion').on('click', function(e) {
		e.preventDefault();
		var username = $('#username').val();
		var password = $('#password').val();
		$.ajax({
			url : "AuthentificationWebServelt", // Le nom du fichier indiqué
			// dans le formulaire
			type : "post", // La méthode indiquée dans le formulaire (get ou
			// post)
			data : 'login=' + username + '&password=' + password,
			success : function(response) { // Je récupère la réponse du fichier
				window.location.replace("secure/portofolio.jsp");
			},
			error : function(request) {
				$('#authentificationStatus').css("display", "");
				setTimeout(function() {
					$('#authentificationStatus').css("display", "none");
				}, 5000);
			}
		});

	});

	$("#deconnect").click(function() {

		$.ajax({
			type : 'POST',
			url : "DeconnectionServlet",
			success : function(data) {
				window.location.replace("accueil.jsp");
			},
			error : function(data) {
				window.location.replace("accueil.jsp");
			}

		});

	});
});