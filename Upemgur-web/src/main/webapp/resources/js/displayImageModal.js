//Displays Image in a modal

$(document).ready(function() {

	// LOAD IMAGE

	var fileInput = document.getElementById('fileInput');
	var uploadFields = document.getElementById('uploadFields');
	var pictureDisplayZone = document
	.getElementById('pictureDisplayZone');
	var errorBlockPicture = document
	.getElementById('errorBlockPicture');
	var pictureAttrZone = document
	.getElementById('pictureAttrZone');
	var uploadButton = document.getElementById("uploadPicture");

	fileInput
	.addEventListener(
			'change',
			function(e) {
				var file = fileInput.files[0];
				var imageType = /image.*/;

				if (file.type.match(imageType)) {
					var reader = new FileReader();

					reader.onload = function(e) {
						pictureDisplayZone.innerHTML = "";
						pictureDisplayZone.setAttribute("class", "img-thumbnail");

						var img = new Image();
						img.src = reader.result;

						var MAX_WIDTH = 550;
						var MAX_HEIGHT = 550;

						var width = img.width;
						var height = img.height;

						if (width > height) {
							if (width > MAX_WIDTH) {
								height *= MAX_WIDTH
								/ width;
								width = MAX_WIDTH;
							}
						} else {
							if (height > MAX_HEIGHT) {
								width *= MAX_HEIGHT
								/ height;
								height = MAX_HEIGHT;
							}
						}

						img.height = height;
						img.width = width;

						pictureDisplayZone.appendChild(img);
					}

					uploadFields.setAttribute("style", "margin-top: 25px;");

					$("#pictureTitle").val(file.name);

					pictureAttrZone.innerHTML = "<div><p><strong>Fichier : </strong>"
						+ file.name
						+ "</p></div>"
						+ "<div><p><strong>Taille : </strong>"
						+ adjustPictureSize(file.size)
						+ "</p></div>"
						+ "<div><p><strong>Type : </strong>"
						+ file.type
						+ "</p></div>";
					errorBlockPicture.innerHTML = "";
					uploadButton.disabled = false;

					reader.readAsDataURL(file);
				} else {
					errorBlockPicture.innerHTML = "<p class=\"btn btn-danger btn-lg\">" +
					"Veuillez choisir une image !</p>";
					uploadButton.disabled = true; 
				}
			});

	// UPLOAD PICTURE

	$("#uploadPicture").on('click',	function(e) {
		var title = $("#pictureTitle").val();
		var description = $("#pictureDesc").val();
		var fileName = $("#fileInput").val();

		if (checkPictureTitle(title)) {
			errorBlockPicture.innerHTML = "<p class=\"btn btn-success btn-lg\">" +
			"Votre image a été chargée, elle apparait maintenant dans votre portofolio !</p>";
			setTimeout(function(){
				window.location="portofolio.jsp";
			}, 3000);
		} else {
			window.location.reload();
		}
	});
});

//FONCTIONS

function drawImageModal(source, nom, alt) {
	var src = source;
	var name = nom;
	var extension = name.substr(name.lastIndexOf(".") + 1);

	var alt_infos = alt.split(";");
	var idPicture = alt_infos[0];
	var idUser = alt_infos[1];
	var owner = alt_infos[2];
	var current_user = alt_infos[3];
	var role = alt_infos[4];

	if(alt_infos[5] != "")
		var desc = alt_infos[5];	
	else 
		desc = "N/A";

	var height = alt_infos[6];
	var width = alt_infos[7];
	var visibility = alt_infos[8];

	if(visibility == 'PUBLIC'){
		visibility = 'Publique';
		var newVisibility = "PRIVATE";
	}
	else {
		visibility = 'Privée';
		var newVisibility = "PUBLIC";
	}

	var errorStatus = "<div id='errorImageStatus' style='display: none' class='form-group'>" 
		+ "<p class='btn btn-danger btn-lg'>" 
		+ "Un problème est survenu ...</p></div>";
	var img = '<img src="'
		+ src
		+ '" class="img-responsive center-block" style="display: block; margin-left: auto; margin-right: auto;"/><br />';
	var infos = '<strong>'
		+ name
		+ '</strong><br />'
		+ "<br /><strong>Type : </strong>image/"
		+ extension
		+ "<input id='idPicture' value='"+ idPicture + "' style='display: none'>" 
		+ "<input id='idUser' value='"+ idUser + "' style='display: none'>"
		+ "<input id='newVisibility' value='"+ newVisibility + "' style='display: none'>";
	var infosPremium = "<br /><strong>Publiée par : </strong>" 
		+ owner
		+ "<br /><strong>Description : </strong>"
		+ desc
		+ "<br /><strong>Visibilité : </strong>"
		+ visibility
		+ "<br /><strong>Taille de l'image : </strong>"
		+ getImageSizeInBytes(src);
	var dimensionsJPG = "<br /><strong>Dimensions : </strong>"
		+ width
		+ " x "
		+ height;
	var updateButton = '<button id="updateImageVisibility" type="submit" style="margin-right: 1%" ' 
		+ 'onclick="updateImageVisibility()" class="btn btn-info">' 
		+ 'Changer la visibilité</button>';
	var deleteButton = '<button id="deleteImage" type="submit" onclick="suppressImage()"' 
		+ ' class="btn btn-danger">Supprimer l\'image</button>';

	if(role == "PREMIUM")
		infos += infosPremium;

	if(extension == "jpg" || extension == "jpeg"
		|| extension == "JPG" || extension == "JPEG")
		infos += dimensionsJPG;

	if(current_user == owner){
		infos += '<br /><br />';
		if(role == "PREMIUM"){
			infos += updateButton + deleteButton;
		}
		else
			infos += deleteButton;
	}

	$('#myModal').modal();			
	$('#myModal').on(
			'shown.bs.modal',
			function() {
				$('#myModal .modal-body')
				.html(errorStatus + img + infos);
			});
	$('#myModal').on(
			'hidden.bs.modal',
			function() {
				$('#myModal .modal-body')
				.html('');
			});
}

function checkPictureTitle(pictureTitle) {
	if (pictureTitle.length === 0 || !pictureTitle.trim()) {
		document.getElementById("errorBlockPicture").innerHTML = "<p class=\"btn btn-danger btn-lg\">"
			+ "Saisissez un titre valide !</p>";
		return false;
	} else
		return true;
}

function adjustPictureSize(size) {
	if (size >= 1024)
		size /= 1024;

	if (size >= 1024) {
		size /= 1024;
		return size.toFixed(2) + " Mo";
	}
	return size.toFixed(2) + " Ko";
}

function getImageSizeInBytes(imgURL) {
	var request = new XMLHttpRequest();
	request.open("HEAD", imgURL, false);
	request.send(null);
	var headerText = request.getAllResponseHeaders();
	var re = /Content\-Length\s*:\s*(\d+)/i;
	re.exec(headerText);
	return adjustPictureSize(RegExp.$1);
}

function suppressImage(){

	var idPicture = $('#idPicture').val();
	var idUser = $('#idUser').val();

	$.ajax({
		url : "ImageDeleteServlet",
		type : "post",
		data : 'idPicture=' + idPicture + '&idUser=' + idUser,
		success : function(response) {				
			window.location.replace("portofolio.jsp");
		},
		error : function(request) {
			$('#errorImageStatus').css("display", "");
			setTimeout(function() {
				$('#errorImageStatus').css("display", "none");
			}, 3000);
		}

	});
}

function updateImageVisibility(){

	var idPicture = $('#idPicture').val();
	var idUser = $('#idUser').val();
	var newVisibility = $('#newVisibility').val();

	$.ajax({
		url : "ImageUpdateServlet",
		type : "POST",
		data : 'idPicture=' + idPicture + '&idUser=' + idUser + '&newVisibility=' + newVisibility,
		success : function(response) {				
			window.location.replace("portofolio.jsp");
		},
		error : function(request) {
			$('#errorImageStatus').css("display", "");
			setTimeout(function() {
				$('#errorImageStatus').css("display", "none");
			}, 3000);
		}
	});
}