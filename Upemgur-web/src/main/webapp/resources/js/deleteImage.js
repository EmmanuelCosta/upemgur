$(document).ready(function() {
	
	$('#deleteImage').on('click', function(e) {
		e.preventDefault();
		var idPicture = $('#idPicture').val();
		var idUser = $('#idUser').val();
		
		$.ajax({
			url : "ImageDeleteServlet",
			type : "post",
			data : 'idPicture=' + idPicture + '&idUser=' + idUser,
			success : function(response) {
				alert("Votre image a été supprimée.");
				window.location.replace("portofolio.jsp");
			},
			error : function(request) {
				$('#deleteImageStatus').css("display", "");
				setTimeout(function() {
					$('#deleteImageStatus').css("display", "none");
				}, 2000);
			}
		});
		
	});
	
});