/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {


    $('#userForm').on('submit', function (e) {
        e.preventDefault();
        var result = true;

        var $this = $(this); // L'objet jQuery du formulaire

        //syntaxe a utilise si on recupère depuis un formulaire jsf <h:form>
        // var username = $("#userForm\\:username").val();

        var username = $("#userForm\\:username").val();
        var nom = $("#userForm\\:nom").val();
        var pwd = $("#userForm\\:pwd").val();
        var email = $("#userForm\\:email").val();

        var phone = $("#userForm\\:phone");
        var phoneVal = phone.val();

        var typeCompte = $("#typeCompte :selected").text();
        
        if (nom === "") {
            $("#hideNameValidatorMessage").css("display", "none");
            $("#nomObligatoire").css("display", "");
            changeSequenceColor($("#userForm\\:nom"), $("#nomObligatoire"));
            result = false;
        }

        if (username === "") {
            $("#hideUsernameValidatorMessage").css("display", "none");
            $("#usernameObligatoire").css("display", "");
            changeSequenceColor($("#userForm\\:username"), $("#usernameObligatoire"));
            result = false;
        }
        if (pwd === "") {
            $("#hidePwdValidatorMessage").css("display", "none");
            $("#pwdObligatoire").css("display", "");
            changeSequenceColor($("#userForm\\:pwd"), $("#pwdObligatoire"));
            result = false;
        }


        if (email === "" | !isValidEmailAddress(email)) {
            $("#hideEmailValidatorMessage").css("display", "none");
            $("#emailObligatoire").css("display", "");
            changeSequenceColor($("#userForm\\:email"), $("#emailObligatoire"));
            result = false;
        }

        if (phoneVal !== "" && !checkPhone(phoneVal)) {
            $("#hidePhoneValidatorMessage").css("display", "none");
            $("#phoneObligatoire").css("display", "");
            changeSequenceColor(phone, $("#phoneObligatoire"));
            result = false;
        }


        if (result) {
            controlInServerSideAndRedirect(nom, username, pwd, email, phoneVal,typeCompte);
        }


    });

//je fais apparaitre si je perd le focus
    $("#userForm\\:username").on('blur', function () {
        $("#hideUsernameValidatorMessage").css("display", "inline");



    });

    //je fais disparaitre si je clic
    $("#userForm\\:username").on('click', function () {
        $("#hideUsernameValidatorMessage").css("display", "none");

    });

    //je fais apparaitre si je perd le focus
    $("#userForm\\:email").on('blur', function () {
        $("#hideEmailValidatorMessage").css("display", "inline");



    });

    //je fais disparaitre si je clic
    $("#userForm\\:email").on('click', function () {
        $("#hideEmailValidatorMessage").css("display", "none");

    });

    //je fais apparaitre si je perd le focus
    $("#userForm\\:pwd").on('blur', function () {
        $("#hidePwdValidatorMessage").css("display", "inline");



    });

    //je fais disparaitre si je clic
    $("#userForm\\:pwd").on('click', function () {
        $("#hidePwdValidatorMessage").css("display", "none");

    });


//je fais apparaitre si je perd le focus
    $("#userForm\\:phone").on('blur', function () {
        var phone = $("#userForm\\:phone");
        if (phone.val() !== "" & !checkPhone(phone.val())) {
            $("#phoneObligatoire").css("display", "inline");
        }



    });

    //je fais disparaitre si je clic
    $("#userForm\\:phone").on('click', function () {
        $("#phoneObligatoire").css("display", "none");

    });

    //je fais disparaitre si je clic
    $("#userForm\\:nom").on('click', function () {
        $("#hideNameValidatorMessage").css("display", "none");

    });

    //je fais apparaitre si je perd le focus
    $("#userForm\\:nom").on('blur', function () {
        $("#hideNameValidatorMessage").css("display", "inline");



    });
    function controlInServerSideAndRedirect(nom, username, pwd, email, phoneVal,typeCompte) {


        $.ajax({
            url: "InscriptionServlet", // Le nom du fichier indiqué dans le formulaire
            type: "post", // La méthode indiquée dans le formulaire (get ou post)
            data: 'nom=' + nom + '&username=' + username + '&pwd=' + pwd + '&email=' + email + '&phone=' + phoneVal+'&typeCompte='+typeCompte,
            success: function (response) { // Je récupère la réponse du fichier 
//              alert("succes");
                $('#inscriptionOK').css("display","");
                setTimeout(function (){
                    window.location = "connexion.jsp";
                },3000);
            },
            error: function (request) {
                var errorNom = request.getResponseHeader("nom");
                var errorUsername = request.getResponseHeader("username");
                var errorPwd = request.getResponseHeader("pwd");
                var errorEmail = request.getResponseHeader("email");
                var errorPhone = request.getResponseHeader("phone");
                
//                var errorMessage=errorNom+" 1 "+errorUsername+" 2 "+errorPwd+" 3 "+errorEmail+" 4 "+errorPhone+" fin";
//                alert(errorMessage);
                if (errorNom != null && errorNom !== "") {
                    $("#hideNameValidatorMessage").css("display", "none");
                    $("#nomObligatoire").css("display", "");
                    changeSequenceColor($("#userForm\\:nom"), $("#nomObligatoire"));
                }
                if (errorEmail != null && errorEmail !== "") {
                    $("#hideEmailValidatorMessage").css("display", "none");
                    $("#emailObligatoire").css("display", "");
                    changeSequenceColor($("#userForm\\:email"), $("#emailObligatoire"));
                }

                if (errorUsername != null && errorUsername !== "") {
                    $("#hideUsernameValidatorMessage").css("display", "none");
                    $("#usernameObligatoire").css("display", "");
                    changeSequenceColor($("#userForm\\:username"), $("#usernameObligatoire"));
                }
                if (errorPwd != null && errorPwd !== "") {
                    $("#hidePwdValidatorMessage").css("display", "none");
                    $("#pwdObligatoire").css("display", "");
                    changeSequenceColor($("#userForm\\:pwd"), $("#pwdObligatoire"));
                }
                if (errorPhone != null && errorPhone !== "") {
                    $("#hidePhoneValidatorMessage").css("display", "none");
                    $("#phoneObligatoire").css("display", "");
                    changeSequenceColor($("#userForm\\:phone"), $("#phoneObligatoire"));
                }
            }
        });
    }
    function checkPhone(data) {

        return /^-?[\d.]+(?:e-?\d+)?$/.test(data);
    }


    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }


    function setBackgroundColor(p, c) {

        p.css("background-color", c);
    }
    var i = 0;
    function changeSequenceColor(p, toHide) {
        setBackgroundColor(p, "#E8E8E8 ");
//        setTimeout(function () {
//            setBackgroundColor(p, "white");
//        }, 5000);
//    $("#phoneObligatoire").html("new message");
        p.click(function () {
            setBackgroundColor(p, "white");
            toHide.css("display", "none");
        });
    }
});


