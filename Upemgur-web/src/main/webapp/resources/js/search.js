$(document).ready(function(){	
	$('[data-toggle="tooltip"]').tooltip();

	$("#searchField").keyup(function() {
		var title = $("#searchField").val();
		var userId = $("#userIdInput").val();
		$.ajax({
			url: "SearchServlet",
			method: "GET",
			data: "title=" + title + "&userId=" + userId,
			success: function(response){
				$("#searchPictures").html(response);
			},
			error: function(request){

			}
		});
	})
})

function search(){
	var title = $("#searchField").val();
	var userId = $("#userIdInput").val();

	$.ajax({
		url: "SearchServlet",
		method: "GET",
		data: "title=" + title + "&userId=" + userId,
		success: function(response){
			$("#searchPictures").html(response);
		},
		error: function(request){

		}
	});
}